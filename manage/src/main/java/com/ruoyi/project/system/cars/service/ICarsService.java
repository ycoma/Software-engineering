package com.ruoyi.project.system.cars.service;

import java.util.List;
import com.ruoyi.project.system.cars.domain.Cars;

/**
 * 车辆管理Service接口
 * 
 * @author ruoyi
 * @date 2020-06-14
 */
public interface ICarsService 
{
    /**
     * 查询车辆管理
     * 
     * @param idcars 车辆管理ID
     * @return 车辆管理
     */
    public Cars selectCarsById(Long idcars);

    /**
     * 查询车辆管理列表
     * 
     * @param cars 车辆管理
     * @return 车辆管理集合
     */
    public List<Cars> selectCarsList(Cars cars);

    /**
     * 新增车辆管理
     * 
     * @param cars 车辆管理
     * @return 结果
     */
    public int insertCars(Cars cars);

    /**
     * 修改车辆管理
     * 
     * @param cars 车辆管理
     * @return 结果
     */
    public int updateCars(Cars cars);

    /**
     * 批量删除车辆管理
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    public int deleteCarsByIds(String ids);

    /**
     * 删除车辆管理信息
     * 
     * @param idcars 车辆管理ID
     * @return 结果
     */
    public int deleteCarsById(Long idcars);
}

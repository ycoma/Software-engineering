# Zey更新日志

<u>第一次做前端，日常拖延症耽误了一些进度，准备从这周开始每天争取完成一个页面吧</u>

## 5.17

#### 已完成：

图片及主页面设计

其中一部分展示：

​	![avatar]( img/example/5.18/1.png)

![avatar]( img/example/5.18/2.png)

![avatar]( img/example/5.18/3.png)

![avatar]( img/example/5.18/4.png)

#### 明日任务

修改一下主界面的一些功能完善，做一下商品详情页面和购物车的一些构想，如果拖延症没爆发争取多完成一些。

## 5.18

#### 已完成：

晚间花了点时间完成了商品栏初步设计

![avatar]( img/example/5.19/1.png)

#### 明日任务：	

完成商品栏的另外一个布局，和商品详情，购物车界面如果还能有时间可以争取一下。

## 5.19

#### 已完成：

完成了商品栏另外一种布局，商品详情的简单设计，购物车界面，发表评论界面

![avatar]( img/example/5.20/6.png)

![avatar]( img/example/5.20/3.png)

![avatar]( img/example/5.20/4.png)



![avatar]( img/example/5.20/5.png)

![avatar]( img/example/5.20/1.png)



![avatar]( img/example/5.20/2.png)

#### 6.15-今：	

1.完成了一些Html文档的补充和修改，包括404界面，跳转界面，文章详情和追踪订单等。

2.测试样例的撰写。


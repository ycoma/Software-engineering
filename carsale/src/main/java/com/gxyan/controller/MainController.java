package com.gxyan.controller;

import org.springframework.boot.autoconfigure.web.servlet.WebMvcAutoConfiguration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * @author djhz
 * @date 2020.6.16
 */
@Controller
public class MainController{
    /**
     * 默认主页
     *
     * @return
     */
    /**@RequestMapping("/")
    public String main() {
        return "main";
    }

    /**
     * 跳转到主页
     * @return
     */
    @RequestMapping("/main")
    public String turnmain() {
        return "main";
    }


    /**
     * 跳转到商品详情页面
     * @return
     */
    @RequestMapping("/details")
    public String details(){
        return "details";
    }

    /**
     * 跳转购物车页面
     * @return
     */
    @RequestMapping("/cart")
    public String cart(){
        return "cart";
    }

    /**
     * 跳转到评论页面
     * @return
     */
    @RequestMapping("/comment")
    public String comment(){
        return "comment";
    }

    /**
     * 跳转到汽车页面
     * @return
     */
    @RequestMapping("/car")
    public String car(){
        return "car";
    }

    /**
     * 跳转到登陆页面
     * @return
     */
    @RequestMapping("/login")
    public String login(){
        return "login";
    }

    /**
     * 跳转到注册页面
     * @return
     */
    @RequestMapping("/register")
    public String register(){
        return "register";
    }

}

package com.ruoyi.project.system.cars.domain;

import java.util.Date;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;
import com.ruoyi.framework.aspectj.lang.annotation.Excel;
import com.ruoyi.framework.web.domain.BaseEntity;

/**
 * 车辆管理对象 cars
 * 
 * @author ruoyi
 * @date 2020-06-14
 */
public class Cars extends BaseEntity
{
    private static final long serialVersionUID = 1L;

    /** 车辆编号 */
    private Long idcars;

    /** 车辆名称 */
    @Excel(name = "车辆名称")
    private String carName;

    /** 车辆机型 */
    @Excel(name = "车辆机型")
    private String carType;

    /** 车辆年份 */
    @Excel(name = "车辆年份", width = 30, dateFormat = "yyyy-MM-dd")
    private Date carData;

    /** 车辆库存 */
    @Excel(name = "车辆库存")
    private Long carNum;

    /** 车辆状态 */
    @Excel(name = "车辆状态")
    private String carState;

    /** 车辆颜色 */
    @Excel(name = "车辆颜色")
    private String carColor;

    /** 车辆价格 */
    @Excel(name = "车辆价格")
    private Long carPrice;

    /** 车辆配置 */
    @Excel(name = "车辆配置")
    private String carConfig;

    public void setIdcars(Long idcars) 
    {
        this.idcars = idcars;
    }

    public Long getIdcars() 
    {
        return idcars;
    }
    public void setCarName(String carName) 
    {
        this.carName = carName;
    }

    public String getCarName() 
    {
        return carName;
    }
    public void setCarType(String carType) 
    {
        this.carType = carType;
    }

    public String getCarType() 
    {
        return carType;
    }
    public void setCarData(Date carData) 
    {
        this.carData = carData;
    }

    public Date getCarData() 
    {
        return carData;
    }
    public void setCarNum(Long carNum) 
    {
        this.carNum = carNum;
    }

    public Long getCarNum() 
    {
        return carNum;
    }
    public void setCarState(String carState) 
    {
        this.carState = carState;
    }

    public String getCarState() 
    {
        return carState;
    }
    public void setCarColor(String carColor) 
    {
        this.carColor = carColor;
    }

    public String getCarColor() 
    {
        return carColor;
    }
    public void setCarPrice(Long carPrice) 
    {
        this.carPrice = carPrice;
    }

    public Long getCarPrice() 
    {
        return carPrice;
    }
    public void setCarConfig(String carConfig) 
    {
        this.carConfig = carConfig;
    }

    public String getCarConfig() 
    {
        return carConfig;
    }

    @Override
    public String toString() {
        return new ToStringBuilder(this,ToStringStyle.MULTI_LINE_STYLE)
            .append("idcars", getIdcars())
            .append("carName", getCarName())
            .append("carType", getCarType())
            .append("carData", getCarData())
            .append("carNum", getCarNum())
            .append("carState", getCarState())
            .append("carColor", getCarColor())
            .append("carPrice", getCarPrice())
            .append("carConfig", getCarConfig())
            .toString();
    }
}

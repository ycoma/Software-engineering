package com.gxyan.dao;

import com.gxyan.pojo.Car;
import com.gxyan.pojo.Employee;
import com.gxyan.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface getCarMapper {
    @Select("select * from car where id = #{id}")
    public Car getCarById(Long id);

    @Select("select * from customer where username = #{name} and password = #{password}")
    public User getUserBynameBypassword(String name, String password);
}

package com.gxyan.controller;

import com.gxyan.dao.CustomerMapper;
import com.gxyan.dao.getuserMapper;
import com.gxyan.pojo.Customer;
import com.gxyan.pojo.Employee;
import com.gxyan.pojo.User;
import org.apache.ibatis.jdbc.Null;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.websocket.server.PathParam;

@Controller


public class LoginCheckController {

    @Autowired
    getuserMapper getUser;

    @RequestMapping("/checkLogin")
    public String check(@PathParam("username") String username, @PathParam("password") String password){


        User user= getUser.getUserBynameBypassword(username,password);

        if(user== null){
            return "login";

        }{
            return "main";
        }

    }


    @RequestMapping("/userregister")
    public String register(@PathParam("username") String username, @PathParam("password") String password){

        int resultCount = getUser.insertUser(username,password);
        if (resultCount != 0) {
            return "login";
        }
        return "register";

    }
}

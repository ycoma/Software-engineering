package com.ruoyi.project.system.cars.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.framework.aspectj.lang.annotation.Log;
import com.ruoyi.framework.aspectj.lang.enums.BusinessType;
import com.ruoyi.project.system.cars.domain.Cars;
import com.ruoyi.project.system.cars.service.ICarsService;
import com.ruoyi.framework.web.controller.BaseController;
import com.ruoyi.framework.web.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.framework.web.page.TableDataInfo;

/**
 * 车辆管理Controller
 * 
 * @author ruoyi
 * @date 2020-06-14
 */
@Controller
@RequestMapping("/system/cars")
public class CarsController extends BaseController
{
    private String prefix = "system/cars";

    @Autowired
    private ICarsService carsService;

    @RequiresPermissions("system:cars:view")
    @GetMapping()
    public String cars()
    {
        return prefix + "/cars";
    }

    /**
     * 查询车辆管理列表
     */
    @RequiresPermissions("system:cars:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Cars cars)
    {
        startPage();
        List<Cars> list = carsService.selectCarsList(cars);
        return getDataTable(list);
    }

    /**
     * 导出车辆管理列表
     */
    @RequiresPermissions("system:cars:export")
    @Log(title = "车辆管理", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Cars cars)
    {
        List<Cars> list = carsService.selectCarsList(cars);
        ExcelUtil<Cars> util = new ExcelUtil<Cars>(Cars.class);
        return util.exportExcel(list, "cars");
    }

    /**
     * 新增车辆管理
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存车辆管理
     */
    @RequiresPermissions("system:cars:add")
    @Log(title = "车辆管理", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Cars cars)
    {
        return toAjax(carsService.insertCars(cars));
    }

    /**
     * 修改车辆管理
     */
    @GetMapping("/edit/{idcars}")
    public String edit(@PathVariable("idcars") Long idcars, ModelMap mmap)
    {
        Cars cars = carsService.selectCarsById(idcars);
        mmap.put("cars", cars);
        return prefix + "/edit";
    }

    /**
     * 修改保存车辆管理
     */
    @RequiresPermissions("system:cars:edit")
    @Log(title = "车辆管理", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Cars cars)
    {
        return toAjax(carsService.updateCars(cars));
    }

    /**
     * 删除车辆管理
     */
    @RequiresPermissions("system:cars:remove")
    @Log(title = "车辆管理", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(carsService.deleteCarsByIds(ids));
    }
}

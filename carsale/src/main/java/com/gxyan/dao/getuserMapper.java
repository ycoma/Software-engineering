package com.gxyan.dao;
import com.gxyan.pojo.Car;
import com.gxyan.pojo.Customer;
import com.gxyan.pojo.Employee;
import com.gxyan.pojo.User;
import com.gxyan.pojo.Car;
import com.gxyan.pojo.Customer;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface getuserMapper {

    @Select("select * from user where username = #{name} and password = #{password}")
    public User getUserBynameBypassword(String name, String password);
    @Insert("INSERT INTO user(username,password) VALUES (#{username},#{password})")
    public int insertUser(String username,String password);
}


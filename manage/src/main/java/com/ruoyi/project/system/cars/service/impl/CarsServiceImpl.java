package com.ruoyi.project.system.cars.service.impl;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.ruoyi.project.system.cars.mapper.CarsMapper;
import com.ruoyi.project.system.cars.domain.Cars;
import com.ruoyi.project.system.cars.service.ICarsService;
import com.ruoyi.common.utils.text.Convert;

/**
 * 车辆管理Service业务层处理
 * 
 * @author ruoyi
 * @date 2020-06-14
 */
@Service
public class CarsServiceImpl implements ICarsService 
{
    @Autowired
    private CarsMapper carsMapper;

    /**
     * 查询车辆管理
     * 
     * @param idcars 车辆管理ID
     * @return 车辆管理
     */
    @Override
    public Cars selectCarsById(Long idcars)
    {
        return carsMapper.selectCarsById(idcars);
    }

    /**
     * 查询车辆管理列表
     * 
     * @param cars 车辆管理
     * @return 车辆管理
     */
    @Override
    public List<Cars> selectCarsList(Cars cars)
    {
        return carsMapper.selectCarsList(cars);
    }

    /**
     * 新增车辆管理
     * 
     * @param cars 车辆管理
     * @return 结果
     */
    @Override
    public int insertCars(Cars cars)
    {
        return carsMapper.insertCars(cars);
    }

    /**
     * 修改车辆管理
     * 
     * @param cars 车辆管理
     * @return 结果
     */
    @Override
    public int updateCars(Cars cars)
    {
        return carsMapper.updateCars(cars);
    }

    /**
     * 删除车辆管理对象
     * 
     * @param ids 需要删除的数据ID
     * @return 结果
     */
    @Override
    public int deleteCarsByIds(String ids)
    {
        return carsMapper.deleteCarsByIds(Convert.toStrArray(ids));
    }

    /**
     * 删除车辆管理信息
     * 
     * @param idcars 车辆管理ID
     * @return 结果
     */
    @Override
    public int deleteCarsById(Long idcars)
    {
        return carsMapper.deleteCarsById(idcars);
    }
}
